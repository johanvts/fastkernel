#include <TensorSketch.h>

using namespace std;
using namespace fftwpp;

TensorSketch::TensorSketch(const unsigned int d_, const unsigned int D_, const unsigned int p_, vector<CountSketch<shark::RealVector>>& CS_array, const double C_): m_d(d_), m_D(D_), m_p(p_)
  ,m_sqrt_C(std::sqrt(C_)),m_FFT_Forward(0),m_FFT_Backward(0)
{ 
  m_CS = CS_array;
  for (unsigned int i=0;i<m_p;i++)
    {
      //These types are aligned arrays of double and complex.
      m_f.push_back(FFTWdouble(m_D));
      m_g.push_back(FFTWComplex(m_D));
    }
  m_FFT_Forward.reset(new rcfft1d(m_D,m_f[0],m_g[0]));
  m_FFT_Backward.reset(new crfft1d(m_D,m_g[0],m_f[0]));
}

TensorSketch::TensorSketch(const unsigned int d_, const unsigned int D_, const unsigned int p_,const double C_):m_d(d_), m_D(D_), m_p(p_)
,m_sqrt_C(std::sqrt(C_))
{
    //select p indentendent hast functions (count sketches)
    vector<CountSketch<shark::RealVector>> CS_stack;
    for(unsigned int p=0;p<m_p;++p){
        CS_stack.push_back(CountSketch<shark::RealVector>(m_d+(C_==0?0:1),m_D));
    }
    m_CS = CS_stack;
  for (unsigned int i=0;i<m_p;i++)
    {
      //These types are aligned arrays of double and complex.
      m_f.push_back(FFTWdouble(m_D));
      m_g.push_back(FFTWComplex(m_D));
    }
  m_FFT_Forward.reset(new rcfft1d(m_D,m_f[0],m_g[0]));
  m_FFT_Backward.reset( new crfft1d(m_D,m_g[0],m_f[0]));
}



shark::RealVector TensorSketch::map(shark::RealVector &in) const
{  
  if(in.size()!=m_d)
  { cout<<"expected: " << m_d <<". got: " << in.size() <<endl;
      throw FastKernelExceptions::DimensionException();
  }

  //append C
  if(m_sqrt_C!=0){
      in.resize(m_d+1);
      in[m_d]=m_sqrt_C;
  }


  //Perform p count sketches and put them in the FFTdouble holder
  for (unsigned int i=0;i<m_p;i++)
    {
      shark::RealVector cs;
      cs = (m_CS[i]).sketchVector(in);
      //copy to FFT holder
      for(unsigned int j = 0; j<m_D;j++)
        {
          m_f[i][j]=cs[j];
        }
  }

  //Transform the p sketches using FFT
  for (unsigned int i=0;i<m_p;i++)
    {
      m_FFT_Forward->fft(m_f[i],m_g[i]);
    }

  //Pairwise multiplication 
  for(unsigned int j=0;j<(m_D/2+1);j++)
    {
      for(unsigned int i=1;i<m_p;i++)
        {
          m_g[0][j]*=m_g[i][j];
        }
  }
 
  //Inverse to find CS(x^p)
  m_FFT_Backward->fftNormalized(m_g[0],m_f[0]);
  shark::RealVector vectorform(m_D);
  std::copy(m_f[0],m_f[0]+m_D,vectorform.begin());
  return vectorform;
  
}

unsigned int TensorSketch::features(){
    return m_D;
}
	     

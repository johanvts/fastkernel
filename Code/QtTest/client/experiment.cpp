#include "experiment.h"
#include "add.h"

using namespace shark;

Experiment::Experiment()
{
}

void Experiment::useRandomData(const int points, const int d_,const double sparse_){
    Uniform_distribution dist(0.0,1.0,d_,sparse_);
    m_dataset = dist.generateDataset(points);
}


void Experiment::print_data(){
    typedef shark::Data<shark::RealVector>::const_batch_reference BatchRef;
    for(auto batch : m_dataset.batches()){
         for(std::size_t x = 0; x != boost::size(batch); ++x){
             shark::RealVector vecX = shark::get(batch,x);
           cout<<common::toString_vector<shark::RealVector>(vecX);
         }
    }
}

void Experiment::useCsvData(const char seperator, const std::string& dataFile, const std::string& labelFile){
    Data<RealVector> inputs;
    Data<RealVector> labels;
    try{
        import_csv(inputs, dataFile, seperator);
    }catch (const std::exception& e) {
        cerr << "Unable to process file " << dataFile << ". Check that the file is available and has correct formatting." << endl;
        exit(EXIT_FAILURE);
    }
    if(labelFile!=""){
        try{
            import_csv(labels, labelFile, seperator);
        } catch (const std::exception& e) {
            cerr << "Unable to process file " << labelFile << ". Check that the file is available and has correct formatting." << endl;
            exit(EXIT_FAILURE);
            }
    }

    if(labelFile!= ""){
        m_labeledData = LabeledData<shark::RealVector,shark::RealVector>(inputs,labels);
    }

    m_dataset= Data<shark::RealVector>(inputs);
}

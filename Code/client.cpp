#include <CountSketch.h>
#include <numeric>
#include <iostream>
#include <complex>
#include <fftw++.h>
#include <Common.h>
#include <math.h>
#include <TS_Map.h>
#include <stdlib.h>  

using namespace std;
using namespace fftwpp;

void conjtest();

int main(int argc, char* argv[])
{
  if(argc<3)
    {
      cout<<"Please provide D,p and x1 x2 x3 ect.\n ex. client 5 2 1 2 3 4 5 6 ect."<<endl;
    }
  else{
    Common cmn;    
    const unsigned int D = atoi(argv[1]);
    const unsigned int p = atoi(argv[2]);
    vector<double> x;
    
    for(int i =3; i <argc; i++)
      {
	x.push_back(atof(argv[i]));
      }    
    x = cmn.normalize_vector(x);
    cmn.print_vector(x);

    TS_Map mapper(x.size(),D,p);
    vector<double> x1 =mapper.map_ts(x);

    cmn.print_vector(x1);
  }
    return 0;
}

double repeat(int k,int D,vector<double>& x)
{
    double sum = 0;
    for(int i=0;i<k;i++)
      {
	TS_Map mapper(x.size(),D,2);
	vector<double> x1 =mapper.map_ts(x);
	sum+=pow(inner_product(x1.begin(),x1.end(),x1.begin(),0.0),2);
      }
    return sum/k;
}

void kernel(vector<double>& x)
{
 cout << "the homogeneus polynomial kernel, <x,x>^2:" << endl;
 cout << fixed << pow(inner_product(x.begin(),x.end(),x.begin(),0.0),2) << endl;    
}

#include <iostream>
#include <iomanip>
#include <string>
#include <map>
#include <random>
#include <cmath>
#include <shark/LinAlg/Base.h>

using namespace shark;

RealVector gaussianVector(unsigned int d);

int main ()
{
  std::map<int,int> hist;
  double mean = 0;
  double stdiv = 1;
  GaussSampler GS(mean,stdiv);
    for(int n=0;n<100;++n){
      ++hist[std::round(GS.sample())];
    }
    for(auto p:hist){
        std::cout<< std::fixed << std::setprecision(1) << std::setw(2)
                 << p.first << ' ' << std::string(p.second/2, '*') << '\n';
    }
    unsigned int d = 10;
    unsigned int D = 5;

    RealMatrix A (D,d,0);
    row(A,0) = gaussianVector(d);
    
    std::cout << A << std::endl;

    return 0;
}

RealVector gaussianVector(unsigned int d){
  //I don't really want to start my random device every time.  
  std::random_device rd;
  std::mt19937 gen(rd());
  std::normal_distribution<> gaussDist(0,1);

  RealVector result(d);
  
  for(unsigned int i=0;i<d;++i){
    result (i) =gaussDist(gen);
  }

  return result;  
}

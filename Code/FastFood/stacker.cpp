#include "stacker.h"


FastFoodStacked::FastFoodStacked(unsigned int d_, unsigned int D_,double sigma_) : m_D(D_), m_d(d_)
{
    //Check given dimensions:
    if(!common::powerOfTwo(m_D) || !common::powerOfTwo(m_d))
        throw FastKernelExceptions::PowerOfTwoException();

    //Check D>=d
    if(!D_>=d_) throw FastKernelExceptions::FastFoodFeatureSizeException();

    //How many V's are needed on stack?
    //If D<d we need just one
    m_D_div_d = m_D/m_d;

    //Iniatilize the stack
    for(unsigned int i=0;i<m_D_div_d;++i){
        m_V.push_back(FastFood(m_d,sigma_));
    }

    //set normalization constant once.
    m_normalization_const = std::pow(m_D,-0.5);

    //b is used in the alternative mapping
    std::mt19937 mt;
    std::uniform_real_distribution<> uni_dist(0,6.28318530717);

    m_b = uni_dist(mt);

}

FastFoodStacked::FastFoodStacked(vector<FastFood> V_): m_V(V_)
{
    m_D=m_V.size()*m_V[0].dimension();
    m_normalization_const = std::pow(m_D,-0.5);
}

void FastFoodStacked::print_operators(){
    for(int i=0;i<m_V.size();++i){
        cout<<"Stack: "<<i<<endl;
        m_V[i].print_operators();
        cout<<"==================="<<endl;
    }
}

///
/// \brief map
/// \param x the input vector
/// \return
///Runs an input throught the stack to give Vx
shark::RealVector FastFoodStacked::map(const shark::RealVector& x) const{

    //Check input dimensions:
    if(!common::powerOfTwo(x.size()))
        throw FastKernelExceptions::PowerOfTwoException();
    if(x.size()!=m_d)
        throw FastKernelExceptions::DimensionException();

    shark::RealVector Vxed = this->Vx(x);
    shark::RealVector Xed = this->normalize_exp(Vxed);

    return Xed;

}

//Full mapping in one iteration. Harder to test than 2 steps (Vx,normalize_exp), but should be faster.

shark::RealVector FastFoodStacked::Vx(const shark::RealVector& x) const{
    shark::RealVector result(m_D);
      for(unsigned int i=0;i<m_V.size();++i){
              shark::RealVector ViX = m_V[i](x);
              std::copy(ViX.begin(),ViX.end(),result.begin()+i*x.size());
      }
      return result;
}

shark::RealVector FastFoodStacked::normalize_exp(shark::RealVector& input) const{
    shark::RealVector mapped(input.size()*2);
    for(unsigned int i=0;i<input.size();++i){
        mapped[i]              =m_normalization_const*std::cos(input[i]);
        mapped[i+input.size()] =m_normalization_const*std::sin(input[i]);
    }
    return mapped;
}

shark::RealVector FastFoodStacked::normalize_2cos_exp(shark::RealVector& input) const{
    shark::RealVector mapped(input.size());
    for(unsigned int i=0;i<input.size();++i){
        mapped[i]              =m_normalization_const*std::sqrt(2)*std::cos(input[i]+m_b);
    }
    return mapped;
}

unsigned int FastFoodStacked::features(){
    return m_D;
}

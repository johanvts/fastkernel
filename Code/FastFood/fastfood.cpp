#include "fastfood.h"


using namespace std;

class DimensionException: public std::exception
{
virtual const char* what() const throw()
{
return "Dimesions didn't match";
}
}dimensionException;

//std::pow(2*M_PI,(d_-1)*0.5)


FastFood::FastFood(unsigned int d_, double sigma_):m_dimension(d_),m_uni_int_dist(0,1),m_gauss(0,1)
  , m_sigma_inverse(std::pow(sigma_,-1))
{
    if(!common::powerOfTwo(d_)) throw FastKernelExceptions::PowerOfTwoException();
    //set B:
    m_B.reserve(m_dimension);
    for (unsigned int i=0;i<m_dimension;++i){
        m_B.push_back(m_uni_int_dist(m_gauss.engine())==0? 1 : -1);
    }
    //set G:
    m_G=m_gauss.gaussSquareDiagonal(m_dimension);

    //set S:
    this->radialGaussianScaling();

    //set permutation seed
    double rand = m_gauss.sample();
    rand *= rand* 12345;
    m_seed =(unsigned int)rand;

}

FastFood::FastFood(vector<double> S_,vector<double> G_, vector<int> B_ ,int PIseed_) : m_gauss(0,1){
    if(!common::powerOfTwo(S_.size())) throw FastKernelExceptions::PowerOfTwoException();
    if(!S_.size()==G_.size()==B_.size()) throw FastKernelExceptions::DimensionException();

    m_S = S_;
    m_G = G_;
    m_B = B_;
    m_seed = PIseed_;
    m_dimension = S_.size();
    m_sigma_inverse = 1.0;

}

//For testing purposes. Normalizez lenghts causing larger map values > corresponing kernel.
void FastFood::normalizationScaling(){
    double frobNormG_sq = 0;
    for (unsigned int i=0;i<m_dimension;++i){
        frobNormG_sq +=m_G[i]*m_G[i];
    }

    for (unsigned int i=0;i<m_dimension;++i){
        //notice this is different from the paper since
        //the paper suggest 1/sqrt(G) not, 1/sqrt(G^2)
        //where G is the Frobenius norm of the G matrix.
        m_S.push_back(std::pow(frobNormG_sq,-0.5));
    }
    m_FrobG = std::sqrt(frobNormG_sq);

}

void FastFood::radialGaussianScaling(){
    //first normalize;
    this->normalizationScaling();
    //take d samples from a N(0,1) gaussian and used the length for each entry in S
    for (int i=0;i<m_dimension;++i){
        double si = 0;
        for (int j=0;j<m_dimension;++j){
            double sample = m_gauss.sample();
            si+= sample*sample;
        }
        m_S[i]=m_S[i]*std::pow(si,0.5);
    }
}

//inplace sign scrambler, B
//O(d)
void FastFood::signScramble(vector<double>& data) const{
    for (unsigned int i=0;i<data.size();++i){
        data[i]*=m_B[i];
    }
}
void FastFood::signScramble(shark::RealVector& data) const{
    for (unsigned int i=0;i<data.size();++i){
        data[i]*=m_B[i];
    }
}

//Random permutation, Pi
//O(d)
void FastFood::permutate(vector<double>& data) const{
    std::shuffle(data.begin(),data.end(),std::default_random_engine(m_seed));
}
void FastFood::permutate(shark::RealVector& data) const{
    std::shuffle(data.begin(),data.end(),std::default_random_engine(m_seed));
}

//Gaussian diagonal, G
//O(d)
void FastFood::gaussDiagonal(vector<double>& data) const{
    for(int i=0;i<m_dimension;++i){
        data[i]*=m_G[i];
    }
}
void FastFood::gaussDiagonal(shark::RealVector& data) const{
    for(int i=0;i<m_dimension;++i){
        data[i]*=m_G[i];
    }
}

//Scaling matrix S
//O(d)
void FastFood::scalingDiagonal(vector<double>& data) const{
    for(int i=0;i<m_dimension;++i){
        data[i]*=m_S[i]*m_sigma_inverse;
    }
}
void FastFood::scalingDiagonal(shark::RealVector& data) const{
    for(int i=0;i<m_dimension;++i){
        data[i]*=m_S[i]*m_sigma_inverse;
    }
}


//G prime delivers the first HG\PiHBd^(-1/2) steps of fastfood in place
//That is everything besides scaling to the desired distribution
//O(d)
void FastFood::vProduct(vector<double>& data) const{
    if(data.size()!=m_dimension) throw FastKernelExceptions::DimensionException();
    //first B (sign scramble
    signScramble(data);
    //perform non-normalized hadamard transform
    m_H.FWHT(data);
    //Permutate
    permutate(data);
    //Apply gaussian diagonal
    gaussDiagonal(data);
    //Then perform a normalized hadamard transform(Hd^(-1/2)
    m_H.FWHTn(data);
    //Scale
    scalingDiagonal(data);
}
void FastFood::vProduct(shark::RealVector& data) const{
    if(data.size()!=m_dimension) throw FastKernelExceptions::DimensionException();
    //first B (sign scramble        
    signScramble(data);
    //perform non-normalized hadamard transform
    m_H.FWHT(data);
    //Permutate
    permutate(data);
    //Apply gaussian diagonal                           
    gaussDiagonal(data);
    //Then perform a normalized hadamard transform(Hd^(-1/2)
    m_H.FWHTn(data);
    //Scale
    scalingDiagonal(data);
}

void FastFood::print_operators(){
    cout<<"B: sign scrambler"<<endl;
    for(int i=0;i<m_dimension;++i){
        for(int j=0;j<m_dimension;++j){
            if(i==j){
                cout<<m_B[j]<<" ";
            }
            else{
                cout<<0<<" ";
            }

            }
        cout<<endl;
    }
    cout<<"----------"<<endl;
    cout<<"PI: permutator"<<endl;
    vector<double> permutator_extract(m_dimension);
    for(int i=0;i<m_dimension;++i){
        permutator_extract[i]=i;
    }
    this->permutate(permutator_extract);
    for(int i=0;i<m_dimension;++i){
        for(int j=0;j<m_dimension;++j){
            if(j==permutator_extract[i]){
                cout<<1;
            }
            else{
                cout<<0;
            }

            }
        cout<<endl;
    }
    cout<<"----------"<<endl;
    cout<<setprecision(3)<<"G: Gaussian diagonal"<<endl;
    for(int i=0;i<m_dimension;++i){
        for(int j=0;j<m_dimension;++j){
            if(i==j){
                cout<<m_G[j]<<" ";
            }
            else{
                cout<<0 << " ";
            }

            }
        cout<<endl;
    }

    cout<<"----------"<<endl;
    cout<<"S: Scaling diagonal"<<endl;
    for(int i=0;i<m_dimension;++i){
        for(int j=0;j<m_dimension;++j){
            if(i==j){
                cout<<m_S[j]<<" ";
            }
            else{
                cout<<0 << " ";
            }

            }
        cout<<endl;
    }
}

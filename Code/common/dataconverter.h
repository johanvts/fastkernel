#ifndef DATACONVERTER_H
#define DATACONVERTER_H
#include <vector>
#include <shark/LinAlg/Base.h>
#include <shark/Data/Dataset.h>

namespace common{
namespace dataconverter
{

    shark::RealVector shark_elementref_to_shark_realVector(const shark::Data<shark::RealVector>::element_reference data);
    std::vector<double> shark_elementref_to_stl_vector(const shark::Data<shark::RealVector>::element_reference data);
    std::vector<double> shark_RealVector_to_stl_vector(const shark::RealVector data);
    shark::RealVector stl_vector_to_shark_RealVector(std::vector<double> data);
}
}

#endif // DATACONVERTER_H

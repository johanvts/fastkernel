#ifndef common_H
#define common_H
#include <vector>
#include <complex>
#include <hadamardmatrix.h>
#include <timer.h>
#include <dataconverter.h>
#include <iostream>

using namespace std;
namespace common{
    void print_FFTWdouble(const double* reals,const unsigned int n);
    void print_FFTWcomplex(const complex<double>* complex,const unsigned int n);
    template <typename T>
    void print_vector(const T& printvector)
    {
      cout  << printvector[0];
      for(unsigned int i =1; i < printvector.size();i++)
          cout << ", " << printvector[i];
        cout << endl;
    }
    template <typename T>
    string toString_vector(const T& printvector){
        if(printvector.size()==0) return "empty";
        stringstream output;
        output << printvector[0];
        for(unsigned int i =1; i < printvector.size();i++)
            output << ", " << printvector[i];
          output << endl;
    return output.str();
    }

    vector <double> tensor_product(const vector <double>& a, const vector <double>& b );
    vector <double> normalize_vector(const vector<double>& vec);
    bool powerOfTwo(int d);
}
#endif




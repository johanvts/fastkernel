#ifndef HADAMARDMATRIX_H
#define HADAMARDMATRIX_H
#include <shark/LinAlg/Base.h>
#include <shark/Data/Dataset.h>
#include <fastkernelexceptions.h>


class HadamardMatrix
{
public:
    HadamardMatrix();
    void populate(unsigned int);
    void populate_n(unsigned int);
    void FWHT(std::vector<double>&) const;
    void FWHT(shark::RealVector&) const;
    void FWHTn(std::vector<double>&) const;
    void FWHTn(shark::RealVector&) const;
    shark::RealMatrix matrix();

    typedef shark::RealVector result_type;

    shark::RealVector operator() (shark::RealVector input) const{
        this->FWHTn(input);
        return input;
    }
private:
    void inline wht_bfly(double&,double&) const;
    int inline log2(int) const;
    shark::RealMatrix  m_matrix;
};

#endif // HADAMARDMATRIX_H

#include "normal_distribution.h"

Normal_distribution::Normal_distribution(const double mean_, const double stdiv_, const int dimensions_)
    : m_mean(mean_),m_stdiv(stdiv_),m_dimensions(dimensions_),m_mt(std::random_device()()), m_gauss_dist(mean_,stdiv_)
{
}

Normal_distribution::~Normal_distribution(){
}

void Normal_distribution::draw(shark::RealVector& input) const{
    input.resize(m_dimensions);
    for(int i=0;i<m_dimensions;++i){
        input(i)=m_gauss_dist(m_mt);
    }
}

#ifndef NORMAL_DISTRIBUTION_H
#define NORMAL_DISTRIBUTION_H
#include <shark/Data/DataDistribution.h>
#include <random>

class Normal_distribution : public shark::DataDistribution<shark::RealVector>
{
public:
    Normal_distribution(const double mean, const double stdiv, const int dimensions);
    ~Normal_distribution();
    void draw(shark::RealVector &input) const;
private:
    double m_mean;
    double m_stdiv;
    int m_dimensions;
    mutable std::mt19937 m_mt;
    mutable std::normal_distribution<> m_gauss_dist;
};

#endif // NORMAL_DISTRIBUTION_H

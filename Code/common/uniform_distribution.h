#ifndef UNIFORM_DISTRIBUTION_H
#define UNIFORM_DISTRIBUTION_H
#include <shark/Data/DataDistribution.h>
#include <random>

class Uniform_distribution : public shark::DataDistribution<shark::RealVector>
{
public:    
    Uniform_distribution(const double from_, const double to_, const int dimensions_,const double sparse_=0);
    ~Uniform_distribution();
    void draw(shark::RealVector &input) const;
private:
    int m_dimensions;
    mutable std::mt19937 m_mt;
    mutable std::uniform_real_distribution<> m_uni_dist;
    double m_sparse;
};

#endif // UNIFORM_DISTRIBUTION_H

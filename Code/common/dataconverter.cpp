#include "dataconverter.h"


namespace common{
namespace dataconverter{


//Methods for converting between shark(ublas) vectors, shark element_references and stl vectors

shark::RealVector shark_elementref_to_shark_realVector(const shark::Data<shark::RealVector>::element_reference data)
{
    shark::RealVector sharkvector(data.size());
    std::copy(data.begin(), data.end(), sharkvector.begin());
    return sharkvector;
}

std::vector<double> shark_elementref_to_stl_vector(const shark::Data<shark::RealVector>::element_reference data){
    std::vector<double> stlvector;
    stlvector.reserve(data.size());
    std::copy(data.begin(), data.end(), std::back_inserter(stlvector));
    return stlvector;
}

std::vector<double> shark_RealVector_to_stl_vector(const shark::RealVector data){
    std::vector<double> stlvector;
    stlvector.reserve(data.size());
    std::copy(data.begin(), data.end(), std::back_inserter(stlvector));
    return stlvector;
}

shark::RealVector stl_vector_to_shark_RealVector(std::vector<double> data){
    shark::RealVector sharkvector(data.size());
    std::copy(data.begin(), data.end(), sharkvector.begin());
    return sharkvector;
}


}
}

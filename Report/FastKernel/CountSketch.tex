\subsection{Count Sketch}
Count sketch is an algorithm for approximating counts of high frequency elements in a stream. It maintains two families of t hash functions. One family $h_t(x):\mathbb{N}\rightarrow[b]$ and another $s_t(x):\mathbb{N}\rightarrow{\pm1}$. For storage it uses an array of t hash tables, each containing b buckets. Each item $x$  encoutered on the stream is run through the t $h(x)$ and $s(x)$ functions. Each $h(x)$ provides an index and each $s(x)$ indicates whether to increment or decrement the counter at that position. The resulting table can be used to approximate a number of properties of the stream. The tensor sketching approach uses a slightly modified version of this algorithm adapted to work on vectors rather than streams:

\begin{definition}[Count Sketch]
Given a vector $\bm{x}\in\mathbb{R}^d$ and two 2-wise independent hash functions $h:\mathbb{N}\rightarrow\{1,...,D\}$ and $s:\mathbb{N}\rightarrow{\pm1}$. The Count Sketch of $\bm{x}$ is defined as $CS(\bm{x}) = \{C_1,...,C_D\}\in\mathbb{R}^D$ where:
\begin{equation}
C_j=\sum_{i\in S_j}s(i)x_i , S_j=\{i\in[d]|h(i)=j\}
\label{eq:CountSketch}
\end{equation}
The iterator set $S_j$ includes all integers $i=\{1,...,d\}$ where $h(i)=j$. 
\end{definition}

\begin{figure}[h!]
\centering
%Use png version for PDF-export\input{ill_CountSketch}
\includegraphics[width=1\textwidth]{CountSketch} 
\caption{An example of a CountSketch with $d=6$ and $D=4$.}
\label{ill:CountSketch}
\end{figure}

Since $h(i)$ maps to only one $j$ computing the entire count sketch thus requires only one run over $\bm{x}$, for each element subtracting or adding its value to the corresponding entry in $CS(\bm{x})$. This means that the Count Sketch can be constructed in linear time in the dimensionality of the  vector, $O(d)$. An example is given in Figure \ref{ill:CountSketch}. This version of the Count Sketch has also been introduced as \emph{The hashing trick}\cite{Weinberger2009}. It has the useful property that the inner product of two vectors is maintained by Count Sketch within a bounded variance.
\begin{lem}
\begin{equation}
E[\langle CS(\bm{x}),CS(\bm{y})\rangle]=\left\langle \bm{x},\bm{y}\right\rangle
\label{lem:CountSketchE}
\end{equation}
\end{lem}
\begin{proof}
Given $\bm{x},\bm{y}\in\mathbb{R}^d$ and Count Sketch as defined in \ref{eq:CountSketch}.
\begin{equation} 
\langle CS(\bm{x}),CS(\bm{y})\rangle=\sum_g^D(\sum_{i\in S_g}s(i)x_i)*(\sum_{j\in S_g}s(j)y_j)
\end{equation}
Keeping the same iterator set $S$ we can simplify the expression.

\begin{equation}
\langle CS(\bm{x}),CS(\bm{y})\rangle=\sum_g^D\sum_{i,j\in S_g}s(i)s(j)x_iy_j
\end{equation}
We now recall the definition of $s(i)$. Since $s:\mathbb{N}\rightarrow{\pm1}$ is uniformly distributed:
\begin{equation}
E[s(i)s(j)]  =
\left\{
	\begin{array}{ll}
		1  & \mbox{if } i = j \\
		0  & \mbox{if } i \neq j 
	\end{array}
\right.
\end{equation}
So:
\begin{equation}
E[\langle CS(\bm{x}),CS(\bm{y})\rangle]=\sum_g^D\sum_{i\in S_g}x_iy_i=\sum_i^dx_iy_i=\langle \bm{x},\bm{y} \rangle
\end{equation}
\end{proof}

To see that the variance is tightly bounded we introduce another lemma:
\begin{lem}
\begin{equation}
Var[\langle CS(\bm{x}),CS(\bm{y})\rangle]=\frac{1}{D}(\sum_{i!=j}x_i^2y_j^2+\sum_{i!=j}x_iy_ix_jy_j)
\label{eq:CountSketchVar}
\end{equation}
\end{lem}
\begin{proof}
See \cite{Weinberger2009} Appendix A.
\end{proof}


\subsubsection{Representing the count sketch as a polynomial}
The current representation of the count sketch as a vector of sums(see \ref{eq:CountSketch}) can be changed to a polynomial representation. We construct a polynomial where each term corresponds to an element of the vector. Consider the polynomial $f_x(u)$ below: 

\begin{equation}
\label{eq:countsketch poly}
f_x(u)=\sum^d_{i=1}s(i)x_iu^{h(i)}=a_0u^0+a_1u^1+a_2u^2+\cdots+a_Du^D
\end{equation}

The $a_0$ term corresponds to $\sum_{i\in S_0}s(i)x_i$, the $a_1$ term corresponds to $\sum_{i\in S_1}s(i)x_i$ etc. The coeficcients of the polynomial matches the entries of the vector representation. In this way a $D-1$ degree polynomial can be made that represents the Count Sketch $CS(\bm{x})$:

\begin{definition}[polynomial representation of Count Sketch]
Given a vector $x\in\mathbb{R^d}$ and two 2-wise independent hash functions $h:\mathbb{N}\rightarrow\{1,...,D\}$ and $s:\mathbb{N}\rightarrow{\pm1}$, the polynomial:
\begin{equation}\label{eq:polyform}
p(u)=\sum^n_{i=1}s(i)x_iu^{h(i)}
\end{equation}
represents the Count Sketch of $x$ by its coefficients.
\end{definition}

The polynomial representation of the Count Sketch will be useful in understanding the Tensor Sketching approach.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 

(TeX-add-style-hook "2cosMapAppendix"
 (lambda ()
    (LaTeX-add-labels
     "2cosappendix"
     "fig:speed2cos_n"
     "fig:speed2cos_D"
     "fig:speed2cos_d"
     "fig:accboundby2cos")))


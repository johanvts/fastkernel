(TeX-add-style-hook "KernelApprox"
 (lambda ()
    (LaTeX-add-labels
     "eq:kernelPromise"
     "eq:measure"
     "fig:hadaresult")
    (TeX-run-style-hooks
     "FFRBF")))


(TeX-add-style-hook "RKS"
 (lambda ()
    (LaTeX-add-labels
     "section:RKS"
     "eq:RKSapprox"
     "eq:RKSmap"
     "eq:imgdisap"
     "eq:eulersmapping"
     "eq:RKS2cosmap"
     "eq:normal_map"
     "eq:exp_map"
     "RKS:error"
     "eq:approxguarantee")
    (TeX-run-style-hooks
     "GaussianRKS")))


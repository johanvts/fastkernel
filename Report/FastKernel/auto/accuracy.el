(TeX-add-style-hook "accuracy"
 (lambda ()
    (LaTeX-add-labels
     "eq:kernelPromise"
     "eq:absmeasure"
     "eq:relmeasure"
     "fig:FF165123"
     "fig:-11vs01"
     "experiement:increasingD"
     "fig:FFTSerror"
     "tab:FFTSerror"
     "fig:FFerror"
     "fig:FFerrorRange"
     "fig:FFerrorDistribution"
     "head:FFmappings"
     "fig:TSsparseError"
     "fig:convert")))


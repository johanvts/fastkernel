\section{Random Kitchen Sinks}\label{section:RKS}

The first technique has been dubbed Fastfood\cite{Le2013} and is a direct extension of the Random Kitchen Sinks framework introduced by Ali Rahimi and Ben Recht\cite{Rahimi2007},\cite{Rahimi2008}. In this section we introduce the Random Kitchen Sink application to approximating shift invariant kernels, in the next we look at the Fastfood extension. Recall that shift invariant kernels consider only the distance between the inputs, and we can generally rewrite them as single input functions $k(\bm{x},\bm{y})=k(\bm{x}-\bm{y})$ like e.g. the Gaussian RBF kernel:

\begin{equation}
  k(\bm{x}-\bm{y})=\exp(-\frac{||\bm{x}-\bm{y}||^2}{2\sigma^2})
\end{equation}

So the purpose of the method will be to construct a mapping $\bm{z(x)}=\bm{x'}$ such that $\langle \bm{x'},\bm{y'}\rangle = e ^{-\frac{||\bm{x}-\bm{y}||^2}{2\sigma^2}}$.

\subsection{Bochner's Theorem}


The mathematical foundation for the Random Kitchen Sink method is Bochner's Theorem.

\begin{thm}[Bochner's theorem\footnote{The theorem was first published in 1932, the version given here is based on \cite{Hinrichs2011},\cite{Summary},\cite{Rahimi2007}}]
Every positive definite function is the Fourier transform of a positive measure. This implies that for any shift invariant kernel $k(\bm{x}-\bm{y})$, there exists a positive measure $\mu$ such that the kernel is the Fourier transform of that measure:

\begin{equation}
k(\bm{x}-\bm{y})=\int_{\mathbb{R}^d}\mu(\bm{\omega})e^{-i\langle\bm{\omega},(\bm{x}-\bm{y})\rangle}d\bm{\omega}
\end{equation}
and the measure $p= \frac{\mu(\bm{\omega})}{\int \mu(\bm{\omega})}$ is a probability measure. 
\end{thm}

If the kernel $k(\bm{x}-\bm{y})$ is properly, scaled Bochner's theorem guarantees that its Fourier transform $p(\bm{\omega})$ is a proper probability distribution\cite{Rahimi2007}.\\


This gives rise to the central step in the Random Kitchen Sinks method. Looking at Bochner's theorem  we are given a measure $p(\bm{\omega})$ that assign weights or probabilities to each $\bm{\omega} \in \mathbb{R}^d$. Since $p(\bm{\omega})$ is a probability distribution $\int p(\bm{\omega})=1$ and we would expect that $k(\bm{x}-\bm{y})\approx e^{-i\langle \bm{\omega}_{\mu},(\bm{x}-\bm{y})\rangle}$ if $\bm{\omega}_{\mu}$ is the average of $p(\bm{\omega})$. Since $E[p(\bm{\omega})]=\bm{\omega}_{\mu}$ we can estimate the kernel by $k(\bm{x}-\bm{y})=E_{\bm{\omega}}[e^{-i\langle \bm{\omega},(\bm{x}-\bm{y})\rangle}]$ where $\bm{\omega}$ is sampled from the $p(\bm{\omega})$ distribution. We can improve the estimate by drawing multiple samples from the distribution $\bm{\omega}_1,\bm{\omega}_2,\cdots,\bm{\omega}_D \sim p(\bm{\omega})$ and averaging their evaluations:

\begin{equation}
E\left[\frac{1}{D}\sum_{j=1}^De^{i\langle \bm{\omega}_j, (\bm{x}-\bm{y})\rangle}\right]=k(\bm{x}-\bm{y})
\label{eq:RKSapprox}
\end{equation}

Since both $p(\bm{\omega})$ and $k(\bm{x}-\bm{y})$ are real we can expect the imaginary part of the approximation to have no contribution and $e^{i\langle \bm{\omega}_j ,(\bm{x}-\bm{y})\rangle }$ can be replaced by $\cos{\bm{\omega}_j^T (\bm{x}-\bm{y}})$.

\begin{equation}
\frac{1}{D}\sum_{j=1}^D\cos{\bm{\omega}_j^T(\bm{x}-\bm{y})}
\end{equation}

We can rewrite this form using the sum of angles rule: $\cos\bm{\omega}_j^T(\bm{x}-\bm{y})=\cos(\bm{\omega}_j^T\bm{x})*\cos(\bm{\omega}_j^T\bm{y})+\sin(\bm{\omega}_j^T\bm{x})*\sin(\bm{\omega}_j^T\bm{y})$. By setting $z_j(x)=\{ \cos{\bm{\omega}_j^T\bm{x}}, \sin{\bm{\omega}_j^T\bm{x}}\}$ we can express the sum as a sum of dot products:

\begin{equation}
E\left[\frac{1}{D}\sum_{j=1}^Dz_j(x)^Tz_j(y)\right]=k(\bm{x}-\bm{y})
\end{equation}  

So to build a mapping $\bm{z}(\bm{x})$ we normalize with respect to the number of samples $D$. $\bm{z}(x)$ consist of $D$ entries where:

\begin{equation}
z_j(x)=\frac{1}{\sqrt{D}}\{ \cos{\bm{\omega}_j^Tx}, \sin{\bm{\omega}_j^Tx}\}
\label{eq:RKSmap}
\end{equation}

The resulting map fulfills our purpose. It approximates the kernel space in $D$ dimensions according to the number of samples drawn. $E[\langle \bm{z}(x),\bm{z}(y)\rangle]=k(\bm{x}-\bm{y})$. We can use Random Kitchen Sinks to find a low dimensional random feature space that approximates any shift-invariant kernel.
\subsection{Alternative mappings}

We may also follow a more direct route from eq.\ref{eq:RKSapprox} by splitting the sum to get:

\begin{equation}
\frac{1}{D}\sum_{j=1}^De^{i\bm{\omega}_j(\bm{x}-\bm{y})}=\frac{1}{D}\sum_{j=1}^De^{i\bm{\omega}_j(\bm{x})}\overline{e^{i\bm{\omega}_j(\bm{y})}}
\end{equation} 

Looking at a single term of the sum:

\begin{eqnarray}
  \label{eq:imgdisap}
  e^{i\bm{\omega}_j(\bm{x})}\overline{e^{i\bm{\omega}_j(\bm{y})}}&=&(\cos{\langle\bm{\omega}_j,\bm{x}\rangle}+i*\sin{\langle\bm{\omega}_j,\bm{x}\rangle})*(\cos{\langle\bm{\omega}_j,\bm{y}\rangle}-i*\sin{\langle \bm{\omega}_j,\bm{y}\rangle}) \nonumber \\
   &=&\cos{\langle\bm{\omega}_j,\bm{x}\rangle}\cos{\langle\bm{\omega}_j,\bm{y}\rangle}+\sin{\langle\bm{\omega}_j,\bm{x}\rangle}\sin{\langle\bm{\omega}_j,\bm{y}\rangle}+img \nonumber\\
 &=&z_j(x)^Tz_j(y)   
\end{eqnarray}
 
As before the imaginary part, $img$, is expected to give zero contribution. Looking at \ref{eq:imgdisap} we see that if we take the complex conjugate of one of the mappings when evaluating the inner product the mapping in \ref{eq:RKSmap} is equivalent to a mapping $\bm{z'}$ with $D$ entries where:

\begin{equation}
  \label{eq:eulersmapping}
  z'_j(x)=\frac{1}{\sqrt{D}}e^{i\bm{\omega}_jx}
\end{equation}

We show both of these mappings here because the $\bm{z}$ and $\bm{z'}$ forms are used interchangeably in \cite{Le2013} and \cite{Rahimi2007} but do have an important difference in the latter requiring a conjugation when taking inner products. Ali Rahimi and Ben Recht further suggest using a mapping: 
\begin{equation}
\label{eq:RKS2cosmap}
z''_j(x)=\frac{1}{\sqrt{D}}\sqrt{2}\cos(\bm{\bm{\omega}_j}^tx+b)
\end{equation}
where $b$ is drawn uniformly from $[0,2\pi]$ \cite{Rahimi2007}. In our implementation we have used the $\bm{z}$ mapping. It has a clear benefit over the $z'$ mapping in that it can be implemented without the use of complex numbers. However, the $\bm{z}$ mapping requires storing both the sine and cosine for each sample $\bm{\omega}$, effectively doubling the feature dimensionality $D$. The $\bm{z''}$ mapping avoids this cost but unfortunately little mathematical evidence is provided in favor of this mapping\footnote{There exist at least two different version of the 2007 paper, putting different emphasis on this mapping.} and experiments also showed the $\bm{z}$ mapping to be more accurate in practice. See p.\pageref{head:FFmappings}.


\subsection{Matrix representation}

The main computations in these mappings are the dot products like $\bm{\omega}_j^T\bm{x}$. To perform the mapping we construct a matrix $\Omega\in\mathbb{R}^{D\times d}$ so that these can more easily be found through matrix multiplication:

\begin{equation}
\Omega\bm{x}=
\begin{bmatrix}
\omega_{1,1} & \omega_{1,2}& \cdots &\omega_{1,d} \\
\vdots     & \vdots    & \ddots & \vdots \\
\omega_{D,1} & \omega_{D,2}& \cdots & \omega_{D,d} \\
\end{bmatrix}  * 
\begin{bmatrix}
x_1 \\
\vdots\\
x_d\\
\end{bmatrix}=
\begin{bmatrix}
\bm{\omega_1}^T\bm{x}\\
\vdots\\
\bm{\omega_D}^T\bm{x}\\
\end{bmatrix}
\end{equation}

Using the $\Omega$ matrix we can map the data in $O(Dd)$ using:
\begin{equation}
\label{eq:normal_map}
z_j(x)=\frac{1}{\sqrt{D}}\{\cos([\Omega\bm{x}]_j),\sin([\Omega\bm{x}]_j)\}
\end{equation}

Or for the $\bm{z'}$ map:

\begin{equation}
\label{eq:exp_map}
z'_j(\bm{x})=\frac{1}{\sqrt{D}}\exp(i[\Omega\bm{x}]_j)
\end{equation}

The matrix representation makes it clear how mapping can be performed in $O(Dd)$ through matrix multiplication.

\input{GaussianRKS}


\subsection{Error Bound}\label{RKS:error}

Hoeffdings inequality guarantees a fast convergence between the kernel and the approximation\cite{Rahimi2007}:

\begin{equation}\label{eq:approxguarantee}
Pr[|\langle \bm{z}(x),\bm{z}(\bm{y})\rangle - k(\bm{x},\bm{y})| \geq \epsilon]\leq 2 \exp{\frac{-D \epsilon^2}{4}}
\end{equation}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 

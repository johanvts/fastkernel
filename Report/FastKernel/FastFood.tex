
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 
\newpage
\section{Fastfood}

The key idea of Fastfood is to replace the $\Omega$ matrix of Random Kitchen Sinks method with another matrix, $V$. Specifically one with properties that allow for very fast vector multiplication while still delivering a mapping comparable to using $\Omega$. The Fast Walsh Hadamard Transform (FWHT) allows us to multiply with a Hadamard matrix in log-linear time\cite{Honda1976}. This allows us to exploit the fact that Hadamard matrices, when combined with diagonal Gaussian matrices, have properties very similar to dense Gaussian matrices, like $\Omega$\cite{Le2013}. 



\subsection{Matrix approximation}

Recall that the matrix $\Omega$ from Random Kitchen Sinks is a $D\times d$ Gaussian random matrix. $d$ is the dimensionality of the input space and each $\bm{\omega}_{i\in\{1..D\}}$ is an independent sample from a $d$ dimensional Gaussian. To approximate the $\Omega$ matrix we construct $V$:

\begin{equation}
\Omega\approx V= \frac{1}{\sigma\sqrt{d}}SHG\Pi HB
\end{equation}

Where $d=2^l$ for some $l\in\mathbb{N}$.

The construction looks complex but the underlying idea is simple. A number of samples are drawn and then combined through a series of permutation, transforms and scaling to form a matrix which resembles $\Omega$. To see how the process works we will examine the components used to form $V$.

\subsubsection{Components}

The matrix $V$ is a product of several matrices:
\begin{description}
\item[Diagonal random matrices:]\hfill \\
The matrices $S,G$ and $B$ are diagonal random matrices. $B$ has random $\pm{1}$ values on its main diagonal, $G$ has random Gaussian entries and $S$ is a random scaling matrix. All of these are computed once and then stored. 
\item[A permutation matrix:]\hfill \\
The $\Pi$ matrix is a random permutation matrix $\Pi\in\{0,1\}^{d x d}$. It can be implemented as a lookup table by sorting random numbers.
\item[The Walsh-Hadamard matrix:]\hfill \\
$H$ is the Walsh-Hadamard matrix:

\begin{definition}[Walsh-Hadamard Matrix]\hfill \\
$H_2=\begin{bmatrix}
           1 & 1 \\ 
           1 & -1 
         \end{bmatrix}$ and 
$H_{2d}=\begin{bmatrix} 
            H_d & H_d \\ 
            H_d & -H_d 
            \end{bmatrix}$\hfill \\
The matrix is only defined for powers of 2. With a normalization factor $d^{-\frac{1}{2}}$ it forms the Walsh-Hadamard Transform\cite{Honda1976}.
\end{definition}
\end{description}

\subsection{Constructing the approximation}

\subsubsection{Sampling and transforming}
The random Kitchen Sinks method rely on samples from a distribution matching the kernel of interest. In the Fastfood method we use a combination of samples from a normal distribution $\mathcal{N}(0,1)$ on the diagonal of $G$ and samples from a spectrum depending on the kernel of interest on the diagonal of $S$\cite{Le2013}.
\subsubsection{The Walsh-Hadamard transform}
The Walsh-Hadamard transform $\frac{1}{\sqrt{d}}H$ is closely related to the Fourier Transform\cite{Kunz1979,Honda1976}. It decomposes an arbitrary vector $\bm{x}\in\mathbb{R}^d$ into a superposition of Walsh functions. This makes a sparse input vector more dense\cite{Ailon2010}. In Fastfood it rotates the Gaussians of $G$ resulting in a denser matrix more similar to a fully random Gaussian matrix, like $\Omega$. Equation \ref{eq:HadamardExample} shows an example for a $d=2, HG$ computation.

\begin{equation}
  \label{eq:HadamardExample}HG=
  \begin{bmatrix}
1 & 1 \\ 
1 & -1
\end{bmatrix}*\begin{bmatrix}
g_1 & 0 \\ 
0 & g_2
\end{bmatrix}=\begin{bmatrix}
g_1 & g_2\\ 
g_1 & -g_2 
\end{bmatrix}
\end{equation}

This is done twice through $\frac{1}{\sqrt{d}}HG\Pi H$. The permutation matrix $\Pi$ ensures that the order of the decomposition is scrambled so no single $G_{ij}$ gets too influential.

A longer chain of Walsh-Hadamards and permutations will bring the result even closer to a fully random Gaussian matrix, but it has been shown that two steps are enough to provide a sufficient amount of decorrelation\cite{Le2013}.

Disregarding the normalization, this forms a matrix $G'$:
\begin{equation}
  \label{eq:Gprime}
  G'=HG\Pi HB
\end{equation}

Each entry $G'_{ij}$ is a result of adding and subtracting zero-mean independent Gaussian random variables. Since sign changes retain Gaussian properties, each entry in $G'$ is a zero-mean Gaussian. The $B$ matrix ensures that there is no correlation between the entries of each row, thus any row of $G'$ is i.i.d. Gaussian. The entries can be calculated by $G'_{ij}=B_{jj}H_i^TG\Pi H_j$\cite{Le2013}.

\subsubsection{Scaling}

The permutation and sign changes due to $\Pi$ and $B$ do not affect the length of the rows:
\begin{eqnarray}
  \label{eq:Length}
  ||G'_{i}||^2&=&[HG\Pi HB(HG\Pi HB)^T]_{ii},i\in[d] \nonumber\\
               &=&[HGH(HGH)^T]_{ii} \nonumber\\
               &=&||G||_{Frob}^2d
\end{eqnarray}



Because of the nature of the Hadamard transform all rows end up having the same length. One way to see this is to consider the first $HG$ which will always yield a matrix with all elements from $G$ on each row. No matter the permutation and sign inversion to the second $H$ it will maintain the same combination of pairs with same or different signs on each column. The result when combined with the first $HG$ will be rows with $d$ elements. Each element will square to $||G||_{frob}^2+k$ and the $k$'s will always sum to zero. This is illustrated in fig.\ref{fig:SquaresToZero}  

\begin{figure}[h]
\begin{equation}
  \centering
HGH_1=\left[
  \begin{bmatrix}
\textbf{a} & \textbf{b} & \textbf{c} & \textbf{d}\\ 
a & -b & c  & -d \\
a  & b & -c & -d\\
a & -b & -c & d 
\end{bmatrix}*
  \begin{bmatrix}
1 & 1 & 1 & 1\\ 
\textbf{1} & \textbf{-1} & \textbf{1}  & \textbf{-1} \\
\textbf{1}  & \textbf{1} & \textbf{-1} & \textbf{-1}\\
1 & -1 & -1 & 1
\end{bmatrix}\right]_1
\nonumber
\end{equation}
\begin{equation}
=\{(..+b+c) (..-b+c) (..+b-c) (..-b-c)\} \nonumber
\end{equation}
\begin{equation}
||HGH_1||^2=(a^2+b^2+c^2+d^2)d+k=||G||_{frob}^2d \nonumber
\end{equation}
  \caption{\textbf{Properties of HGH} Looking across the colums of the hadamard we see that it contains the same number of adding and subtracting combinations.\\ When squaring the result the first and fourth column will cause additional $2bc$ terms and the second and third $-2bc$ terms. In this way $k$ always sums to zero. This property is not affected by permutation of the rows or sign inversion along the columns.}
  \label{fig:SquaresToZero} 
\end{figure}

\newpage
All the rows of $HG\Pi HB$ have the same length and by scaling with $||G||_{frob}^{-1}d^{-\frac{1}{2}}$ we can get length $1$ rows.\footnote{\cite{Le2013} is very brief on this point and also suggest scaling with $||G||_{frob}^{-\frac{1}{2}}d^{-\frac{1}{2}}$. We have attempted to contact the authors of the paper but with no luck. We have tested both approaches and it appears to be a typo in the original text.} This is an undesired property since we want the matrix to behave like the entries of a Gaussian matrix and this property is a sign of row correlation. Having correlated rows means that the map different entries $z_j(\bm{x})$ will be correlated. Take the extreme case where each row in $V'$ is the same. The result is that each $z_j(x)$ is the same and this corresponds to only drawing a single $\bm{\omega}$ when approximating the integral given by Bochner's theorem. To decorrelate the rows we use the scaling matrix $S$. Using $S$ we can adjust the $V$ matrix depending on the kernel of interest. In this case we are focused on the Gaussian RBF kernel, but we may choose any RBF kernel\cite{Le2013}.

For the Gaussian RBF Le et al.\cite{Le2013} suggest sampling entries in $S$ from a distribution:
\begin{equation}\cite{Le2013}
  \label{eq:LeDistribution}
  (2\pi)^{\frac{d}{2}}A^{-1}_{d-1}r^{d-1}e^{\frac{r^2}{2}}
\end{equation}
While this solution appears elegant it also provides for an unnecessarily complex implementation. Since the purpose in this case is to simulate the behavior of a $d$ dimensional vector with each element sampled from $\mathcal{N}(0,1)$ we implement the scaling matrix as:

\begin{algorithmic}[1]
 \REQUIRE{ $G_{frob}^{-1},N\gets\mathcal{N}(0,1)$}
 \ENSURE{ Scaling matrix diagonals $S_i$}
\FORALL{$i\leq d$}
 \STATE $Length\gets 0$
   \FORALL{$j \leq d$}
    \STATE $Length \gets Length+ N.sample()^2$
   \ENDFOR
   \STATE $S[i] \gets \sqrt{(Length)}*G_{frob}^-1$
\ENDFOR
\RETURN{$S$}
\end{algorithmic}

While this means taking $(d^2)$ samples instead of just $d$ it only needs to be done once when initializing the mapping. The benefit is that the implementation is much simpler and rely on a standard normal distribution already available in most programming languages.

\subsubsection{Normalization}
Finally the matrix is normalized to fit the width of the kernel, $\sigma$, and the dimensionality of the input space, $d$.


\begin{equation}
  \label{eq:normalize}
  \frac{1}{\sigma\sqrt{d}}
\end{equation}

This last step can be added to the $S$ matrix implementation to reduce the run time complexity of the algorithm

\subsubsection{Stacking}

Since $V$ is a square $d\times d$ matrix it cannot directly replace $\Omega$ which is $D\times d$. Normally we will want have many more samples than dimensions of the input space $D\gg d$. To do this we will need to form $D/d$ instances of $V$ and stack them to achieve the desired $D\times d$ size:

\begin{equation}
  \label{eq:StackV}
  V^T=[V_1,V_2,...,V_{D/d}]
\end{equation}

\begin{figure}[h!]
\centering
%Use png version for PDF\input{ill_BuildV}
\includegraphics[width=0.5\textwidth]{BuildV} 
\caption{Building V in the case where D=3d.}
\label{ill:BuildV}
\end{figure}

The resulting matrix $V$ will have properties similar to $\Omega$ but entails several computational advantages.

\subsection{Computational advantages}\label{header:FFspeed}
Using the Fast Walsh-Hadamard Transform it is possible to compute each $H_i\bm{x}$ in $O(d \log d)$ for any vector $\bm{x}\in\mathbb{R}^d$ \cite{Ailon2010}. For $D/d$ blocks the total time for matrix-vector multiplication $H\bm{x}$ becomes $O(D \log d)$. The remaining matrix multiplications in $V$ can all be carried out in linear time in $D$. It is easy to see for the diagonal matrices $G,S,B$ and can be achieved for the $\Pi$ matrix by implementing it as a lookup table\cite{Le2013}. This adds a total storage and operation cost of $4D$ for multiplication. The total operation count of Fastfood is $O(D \log d)$ and it uses $O(D)$ storage.

This is a significant improvement of the $O(Dd)$ CPU and storage cost of Random Kitchen Sinks.\footnote{If $D\ll d$ the Walsh-Hadamard transform will still bound Fastfood to $O(d \log d)$ time and will make the approximation slower than the exact calculation if $D<\log d)$. For practical uses of Fastfood $D\gg d$.}

\subsection{Feature map}

The feature map is the same as derived in Random Kitchen Sinks, eq\ref{eq:RKSmap}, but now replacing the $\Omega$ matrix with $V$.

\begin{equation}
  \label{eq:FastfoodMap}
  z_j(\bm{x})=\frac{1}{\sqrt{D}}\{\cos{[V\bm{x}]_j},\sin{[V\bm{x}]_j}\}
\end{equation}

This would have the same desirable property:  $\langle \bm{z}(\bm{x}),\bm{z}(\bm{y'})\rangle = e ^{-\frac{||\bm{x}-\bm{y}||^2}{2\sigma^2}}$. By changing way we sample the $S$ matrix we can make the mapping match any RBF kernel\cite{Le2013}.
 
\subsection{Error Bound}\label{FF:errorBound}

The error bound for Fastfood closely follows the one given for Random Kitchen Sinks in eq.\ref{eq:approxguarantee}. The approximation error of a single $d \otimes d$ block has been shown to be within a factor $O(\sqrt{\log{d/\delta}})$ of Random Kitchen Sinks for a given error probability $\delta$. See theorem 6\cite{Le2013}. We will not go into further proofs here, but the expected inverse relationship between the error and $D$ is confirmed experimentally in section \ref{experiement:increasingD}.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "FastKernel"
%%% End: 

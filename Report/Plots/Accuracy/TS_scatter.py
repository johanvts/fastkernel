# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv
import sys

with open('Experiments/1TensorSketch_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

kernel = [float(x[3]) for x in data if x[0]== 'TensorSketch' ]
fast = [float(x[4]) for x in data if x[0]== 'TensorSketch' ]



plt.scatter(kernel,fast,s=2,alpha=0.1)

plt.title('TensorSketch estimate juxtaposed with Plynomial Kernel')
plt.xlabel('Kernel Values')
plt.ylabel('Estimates')

plt.savefig('TS_Scatter_'+str(sys.argv[1])+'_'+str(sys.argv[2])+'.png', bbox_inches=0,dpi=300)


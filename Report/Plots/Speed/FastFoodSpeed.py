# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('fastfood.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

axislabels = [float(x[1]) for x in data if x[0]== 'FastFood' ]
fastfood = [float(x[3]) for x in data if x[0] == 'FastFood']
FWHTn = [float(x[3]) for x in data if x[0] == 'FWHTn']


fig, ax = plt.subplots()
ax.plot(axislabels, fastfood, 'b^', label='FastFood transform')
ax.plot(axislabels, fastfood, 'k--', label='_nolegend_')
ax.plot(axislabels, FWHTn, 'ro', label='FWHTn')
ax.plot(axislabels, FWHTn, 'k--', label='_nolegend_')

ax.set_xlim(2, 512)
ax.set_xticks([2,64,128,256,512])

#ax.set_yscale('log',basey=2)
#ax.set_xscale('log',basex=2)

#plt.ticklabel_format(style='sci',axis='y',scilimits=(0,0))
plt.legend(numpoints=1,loc=2)
plt.title('Speed measure of the FastFood algorithm')
plt.xlabel('Input dimensions')
plt.ylabel('Nanoseconds on average')

plt.savefig('fastfoodplot.png', bbox_inches=0)


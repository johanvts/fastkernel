# coding: utf-8
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('Experiments/SpeedTest.txt', 'Ur') as f:
    reader = csv.reader(f, delimiter=',')
    next(reader, None) # skip header
    data = list(tuple(rec) for rec in reader)

n = [int(x[3]) for x in data if x[0]== 'FastFood n' ]
fastfood = [float(x[5])*(10**-6) for x in data if x[0] == 'FastFood n']
TS = [float(x[5])*(10**-6) for x in data if x[0] == 'TS n']


fig, ax = plt.subplots()
ax.plot(n, fastfood, 'b^', label='FastFood transform')
ax.plot(n, fastfood, 'k--', label='_nolegend_')
ax.plot(n, TS, 'ro', label='TensorSketch transform')
ax.plot(n, TS, 'k--', label='_nolegend_')

ax.set_xlim(0, 1000000)
ax.set_xticks([0,100000,200000,300000,400000,500000,600000,700000,800000,900000,1000000])

#ax.set_yscale('log',basey=2)
#ax.set_xscale('log',basex=2)

plt.ticklabel_format(style='sci',axis='x',scilimits=(0,0))
plt.legend(numpoints=1,loc=2)
plt.xlabel('Input size, n')
plt.ylabel('Seconds on average')

plt.savefig('Speed_n.png', bbox_inches=0,dpi=300)

